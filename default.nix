{ compiler ? "default", doBenchmark ? false }:

let
  rev = "ee28e35ba37ab285fc29e4a09f26235ffe4123e2";

  pkgs = import
      (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";     }) {};

  f = { mkDerivation, base, data-default, stdenv }:
      mkDerivation {
        pname = "ease";
        version = "0.1.0.0";
        src = ./.;
        libraryHaskellDepends = [ base data-default ];
        description = "Robert Penner's easing equations";
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  drv = haskellPackages.callPackage f {};

in

  if pkgs.lib.inNixShell then drv.env else drv
